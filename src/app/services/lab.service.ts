import { ThrowStmt } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Pokemon } from '../interfaces/pokemon';

@Injectable({ providedIn: 'root' })
export class LabService {
  
  pokemons: Pokemon[] = [];

  /** GET pokemons from the server */
  getPokemons(): Pokemon[]{
    return this.pokemons;
  }

  /** GET pokemons by id */
  getPokemon(id: number): Pokemon {
    return this.pokemons.find(pokemon => pokemon.id === id);
  }  

  addPokemon(pokemon: Pokemon): void{
    this.pokemons.unshift(pokemon);
  }

  deletePokemon(pokemonToFree: Pokemon): void{
    this.pokemons = this.pokemons.filter(pokemon => pokemon !== pokemonToFree);
  }

}
