import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { LabComponent } from '../components/lab/lab.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'lab', component: LabComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
