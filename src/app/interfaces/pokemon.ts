export interface Pokemon {
  id: number;
  pokemonNumber: number;
  category: string;
  race: string;
  name?: string;
  image: string;
  level: number;
  types: Type[];
  description: string;
  weaknesses: Type[];
}

export interface Type{
  id: number;
  name: string;
}