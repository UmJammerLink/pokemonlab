import { Component, Output } from '@angular/core';
import * as EventEmitter from 'events';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title: string = 'Pokémon Rojo / Azul / Amarillo';
  copyright: string = 'Curso Angular DEW II @2021';

}
