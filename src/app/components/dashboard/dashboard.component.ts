import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../../interfaces/pokemon';
import { WildService } from '../../services/wild.service';
import { LabService } from '../../services/lab.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {

  title: string = 'Pokémons en libertad';

  pokemons: Pokemon[] = [];

  constructor(
    private wildService: WildService,
    private labService: LabService
    ) { }

  ngOnInit() {
    this.getPokemons();
  }

  getPokemons(): void {
    this.wildService.getPokemons()
      .subscribe(pokemons => this.pokemons = pokemons.sort((a, b) => a.pokemonNumber - b.pokemonNumber));
  }

  private deletePokemon(pokemon: Pokemon): void {
    this.wildService.deletePokemon(pokemon)
      .subscribe(() => this.getPokemons());
  }

  throwPokeball(pokemon: Pokemon): void{
    this.deletePokemon(pokemon);
    this.labService.addPokemon(pokemon);
  }

}
