import { Component, OnInit } from '@angular/core';

import { Pokemon } from '../../interfaces/pokemon';
import { WildService } from '../../services/wild.service';
import { LabService } from '../../services/lab.service';

@Component({
  selector: 'app-lab',
  templateUrl: './lab.component.html',
  styleUrls: ['./lab.component.css']
})
export class LabComponent implements OnInit {

  title: string = 'Pokémons en el laboratorio';
  pokemons: Pokemon[] = [];

  constructor(
    private labService: LabService,
    private wildService: WildService
  ) { }

  ngOnInit(): void {
    this.getPokemons();
  }

  getPokemons(): void{
    this.pokemons = this.labService.getPokemons();
  }

  private deletePokemon(pokemon: Pokemon): void{
    this.labService.deletePokemon(pokemon);
  }

  freePokemon(pokemon: Pokemon): void{
    this.deletePokemon(pokemon);
    this.getPokemons();
    this.wildService.addPokemon(pokemon)
    .subscribe();
  }

}
